# CTT Testcase Generator Example
Dieses Beispielprojekt wurde im Rahmen einer Bachelorarbeit geschrieben. Es soll den in der Arbeit vorgestellten Ansatz
an einem praktischen Beispiel veranschaulichen. Das Projekt beinhaltet:
* Die zu testende Anwendung: `src/main/resources/EmailPage.html`
* Die Tests:
    * Feature Dateien in `src/test/resources/Feature Files`
    * Step Definition in `src/test/java/StepDefinitions`
* Die Aufgabenmodelle, aus denen die Tests mit Hilfe des Ansatzes aus der Arbeit hergeleitet wurden: `CTT Example/`
    * Dabei zwei Aufgabenmodelle:
      * Originales Aufgabenmodell in den `WriteEmail.*` Dateien
      * Vereinfachte Version in den `WriteEmailSimplified.*` Dateien
    * Dabei für jedes Modell jeweils:
      * `*.ctte` Datei, um es in CTTE zu öffnen 
      * `*.png` Datei für eine grafische Darstellung, ohne es in CTTE öffnen zu müssen
      * `*.xml` Datei für die Eingabe im CTT Testcase Generator ([Hier zu finden](https://git.informatik.uni-rostock.de/eb264/ctt-testcase-generator))

## Hinweise
* Es müssen Mozilla Firefox, Java und Apache Maven installiert sein, damit die Tests ausgeführt werden können
* Dieses Projekt wurde mit Java 17 entwickelt und getestet. Die Kompatibilität mit anderen Java Versionen wurde nicht
  überprüft.
* Je nach Betriebssystem (Linux, Windows, MacOS) wird ein anderer Gecko Treiber geladen und verwendet. Jedoch wurde dies
  nur unter Linux getestet. Die korrekte Ausführung unter Windows und MacOS wurde nicht überprüft.
* Die Versionsnummer von Java, die bei der Eingabe von `mvn -v` im Terminal angezeigt wird, muss übereinstimmen mit der
  Versionsnummer im `<properties>` Element der `pom.xml` Datei.
  * z.B. `mvn -v` zeigt `Java Version: 17.0.2` und im `<properties>` Element steht `17` in der Source- und Targetversion

## Nutzung
1. Lade den Quellcode runter, z.B. mit `git clone`
2. Öffne das Terminal und navigiere mit `cd` zum Wurzelverzeichnis
3. Führe die Tests aus mit `mvn clean test`
4. Nach dem Ende der Tests werden die Ergebnisse im Terminal angezeigt

## Programmteile von Dritten
Dieses Projekt beinhaltet im `src/test/resources/Drivers` Verzeichnis die Gecko Treiber für Linux, Windows und MacOS 
Betriebssysteme, die für die Testdurchführung in Mozilla Firefox benötigt werden. Diese wurden 
[hier](https://github.com/mozilla/geckodriver/releases/tag/v0.30.0) heruntergeladen. Benutzt werden die Inhalte von:
* `geckodriver-v0.30.0-linux64.tar.gz`
* `geckodriver-v0.30.0-macos.tar.gz`
* `geckodriver-v0.30.0-win64.zip`

Diese Gecko Treiber sind lizenziert unter der [Mozilla Public License 2.0](https://www.mozilla.org/en-US/MPL/2.0/).
