Feature: Write Email

  Background:
    Given I open Firefox
    And I open my Email page

  Scenario Outline:
    When I press Write Email button
    And I enter <Recipient> into Recipient field
    And I press Send button
    Then System shows send confirmation

    Examples:
      | Recipient |
      | "Erika@mustermann.de" |
      | "Max@mustermann.de" |

  Scenario Outline:
    When I press Write Email button
    And I enter <Recipient> into Recipient field
    And I enter <Subject> into Subject field
    And I press Send button
    Then System shows send confirmation

    Examples:
      | Recipient | Subject |
      | "Erika@mustermann.de" | "Gute Neuigkeiten!" |
      | "Max@mustermann.de" | "Die Urlaubsfotos" |

  Scenario Outline:
    When I press Write Email button
    And I enter <Recipient> into Recipient field
    And I enter <Message> into Message field
    And I press Send button
    Then System shows send confirmation

    Examples:
      | Recipient | Message |
      | "Erika@mustermann.de" | "Hallo Erika, ich hab im Lotto gewonnen! Liebe Grüße" |
      | "Max@mustermann.de" | "Hallo Max, im Anhang sind die Urlaubsfotos vom letzten Jahr. Viele Grüße" |

  Scenario Outline:
    When I press Write Email button
    And I enter <Recipient> into Recipient field
    And I enter <Subject> into Subject field
    And I enter <Message> into Message field
    And I press Send button
    Then System shows send confirmation

    Examples:
      | Recipient | Subject | Message |
      | "Erika@mustermann.de" | "Gute Neuigkeiten!" | "Hallo Erika, ich hab im Lotto gewonnen! Liebe Grüße" |
      | "Max@mustermann.de" | "Die Urlaubsfotos" | "Hallo Max, im Anhang sind die Urlaubsfotos vom letzten Jahr. Viele Grüße" |

  Scenario Outline:
    When I press Write Email button
    And I enter <Recipient> into Recipient field
    And I press Delete button
    And System asks for confirmation
    And I confirm the deletion
    Then System shows deletion confirmation

    Examples:
      | Recipient |
      | "Erika@mustermann.de" |
      | "Max@mustermann.de" |

  Scenario Outline:
    When I press Write Email button
    And I enter <Recipient> into Recipient field
    And I enter <Subject> into Subject field
    And I press Delete button
    And System asks for confirmation
    And I confirm the deletion
    Then System shows deletion confirmation

    Examples:
      | Recipient | Subject |
      | "Erika@mustermann.de" | "Gute Neuigkeiten!" |
      | "Max@mustermann.de" | "Die Urlaubsfotos" |

  Scenario Outline:
    When I press Write Email button
    And I enter <Recipient> into Recipient field
    And I enter <Message> into Message field
    And I press Delete button
    And System asks for confirmation
    And I confirm the deletion
    Then System shows deletion confirmation

    Examples:
      | Recipient | Message |
      | "Erika@mustermann.de" | "Hallo Erika, ich hab im Lotto gewonnen! Liebe Grüße" |
      | "Max@mustermann.de" | "Hallo Max, im Anhang sind die Urlaubsfotos vom letzten Jahr. Viele Grüße" |

  Scenario Outline:
    When I press Write Email button
    And I enter <Recipient> into Recipient field
    And I enter <Subject> into Subject field
    And I enter <Message> into Message field
    And I press Delete button
    And System asks for confirmation
    And I confirm the deletion
    Then System shows deletion confirmation

    Examples:
      | Recipient | Subject | Message |
      | "Erika@mustermann.de" | "Gute Neuigkeiten!" | "Hallo Erika, ich hab im Lotto gewonnen! Liebe Grüße" |
      | "Max@mustermann.de" | "Die Urlaubsfotos" | "Hallo Max, im Anhang sind die Urlaubsfotos vom letzten Jahr. Viele Grüße" |

