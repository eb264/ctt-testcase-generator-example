package StepDefinitions;

import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.en.*;

import java.io.File;
import java.net.MalformedURLException;
import java.time.Duration;

import org.junit.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WriteEmail {
    WebDriver driver;

    @Given("^I open Firefox$")
    public void openBrowser() {
        if (SystemUtils.IS_OS_WINDOWS) {
            System.setProperty("webdriver.gecko.driver", "src/test/resources/Drivers/geckodriver_win.exe");
        } else if (SystemUtils.IS_OS_MAC) {
            System.setProperty("webdriver.gecko.driver", "src/test/resources/Drivers/geckodriver_macos");
        } else if (SystemUtils.IS_OS_LINUX){
            System.setProperty("webdriver.gecko.driver", "src/test/resources/Drivers/geckodriver_linux");
        }
        driver = new FirefoxDriver();
    }

    @And("^I open my Email page$")
    public void openEmailPage() {
        try {
            String path = new File("src/main/resources/EmailPage.html").toURI().toURL().toString();
            driver.get(path);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @When("^I press Write Email button$")
    public void pressWriteEmail() {
        WebElement writeButton = driver.findElement(new By.ById("write"));
        writeButton.click();

        Assert.assertTrue(!writeButton.isEnabled() &&
                driver.findElement(new By.ById("recipient")).isDisplayed() &&
                driver.findElement(new By.ById("subject")).isDisplayed() &&
                driver.findElement(new By.ById("content")).isDisplayed() &&
                driver.findElement(new By.ById("send")).isDisplayed() &&
                driver.findElement(new By.ById("delete")).isDisplayed());
    }

    @And("I enter {string} into Recipient field")
    public void enterRecipient(String recipient) {
        WebElement recipientField = driver.findElement(new By.ById("recipient"));
        recipientField.sendKeys(recipient);
    }

    @And("I enter {string} into Subject field")
    public void enterSubject(String subject) {
        WebElement recipientField = driver.findElement(new By.ById("subject"));
        recipientField.sendKeys(subject);
    }

    @And("I enter {string} into Message field")
    public void enterMessage(String message) {
        WebElement recipientField = driver.findElement(new By.ById("content"));
        recipientField.sendKeys(message);
    }

    @And("^I press Send button$")
    public void pressSend() {
        WebElement sendButton = driver.findElement(new By.ById("send"));
        sendButton.click();
    }

    @Then("^System shows send confirmation$")
    public void assertSendConfirmation() {
        Alert sendConfirmation = driver.switchTo().alert();
        Assert.assertEquals("E-Mail was sent.", sendConfirmation.getText());
        sendConfirmation.accept();

        Assert.assertFalse(!driver.findElement(new By.ById("write")).isEnabled() ||
                driver.findElement(new By.ById("recipient")).isDisplayed() ||
                driver.findElement(new By.ById("subject")).isDisplayed() ||
                driver.findElement(new By.ById("content")).isDisplayed() ||
                driver.findElement(new By.ById("send")).isDisplayed() ||
                driver.findElement(new By.ById("delete")).isDisplayed());
    }

    @And("^I press Delete button")
    public void pressDelete() {
        WebElement deleteButton = driver.findElement(new By.ById("delete"));
        deleteButton.click();
    }

    @And("^System asks for confirmation$")
    public void assertConfirmationQuestion() {
        Alert confirmDeletion = driver.switchTo().alert();
        Assert.assertEquals("Are you sure you want to delete this E-Mail?", confirmDeletion.getText());
    }

    @And("^I confirm the deletion$")
    public void confirmDeletion() {
        Alert confirmDeletion = driver.switchTo().alert();
        confirmDeletion.accept();
    }

    @Then("^System shows deletion confirmation$")
    public void assertDeletionConfirmation() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.alertIsPresent());

        Alert deletionConfirmation = driver.switchTo().alert();
        Assert.assertEquals("E-Mail was deleted.", deletionConfirmation.getText());
        deletionConfirmation.accept();

        Assert.assertFalse(!driver.findElement(new By.ById("write")).isEnabled() ||
                driver.findElement(new By.ById("recipient")).isDisplayed() ||
                driver.findElement(new By.ById("subject")).isDisplayed() ||
                driver.findElement(new By.ById("content")).isDisplayed() ||
                driver.findElement(new By.ById("send")).isDisplayed() ||
                driver.findElement(new By.ById("delete")).isDisplayed());

    }
}